module.exports = {
	parser: '@typescript-eslint/parser', // Specifies the ESLint parser
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier/@typescript-eslint',
		'plugin:prettier/recommended',
	],
	env: {
		browser: false,
		node: true,
		es6: true,
		jest: true,
	},
	plugins: ['@typescript-eslint'],
	parserOptions: {
		ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
		sourceType: 'module', // Allows for the use of imports
	},
	rules: {
		'@typescript-eslint/explicit-function-return-type': 'off',
		'@typescript-eslint/no-use-before-define': ['error', { functions: false }],
		'@typescript-eslint/no-explicit-any': 'off',
		'@typescript-eslint/camelcase': 'off'
	},
	overrides: [
		// Override some TypeScript rules just for .js files
		{
			files: ['*.js'],
			rules: {
				'@typescript-eslint/no-var-requires': 'off', //
			},
		},
	],
}
