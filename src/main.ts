import { spawn } from 'child_process'

import Koa from 'koa'
import bodyParser from 'koa-bodyparser'

const setUsbState = (state: 'off' | 'on'): Promise<'off' | 'on'> => {
	console.log('Set usb state to', state)
	return new Promise((resolve, reject) => {
		const usb = spawn('uhubctl', ['-l', '1-1', '-p', '2', '-a', state])
		usb.on('close', code => {
			if (code === 0) {
				return resolve(state)
			}
			return reject(
				new Error('Cant toggle usb. uhubctl returned code: ' + code)
			)
		})
	})
}

const coolerState = () => {
	let state: 'off' | 'on' = 'off'
	return {
		get: () => state,
		set: (newState: 'off' | 'on') => {
			state = newState
			return state
		},
	}
}

const sensorsState = () => {
	let state: 'cool' | 'warm' | 'hot' | 'burn' = 'cool'
	return {
		get: () => state,
		set: (newState: 'cool' | 'warm' | 'hot' | 'burn') => {
			state = newState
			return state
		},
	}
}

const criticalTemp = () => {
	let state: 70 | 68 = 70
	return {
		get: () => state,
		set: (newState: 70 | 68) => {
			state = newState
			return state
		},
	}
}

const startServer = async () => {
	const app = new Koa()
	try {
		await setUsbState('off')
	} catch (e) {
		console.error('Cant turn usb off on initialization.', e.message)
	}

	app.context.coolerState = coolerState()
	app.context.sensorsState = sensorsState()
	app.context.criticalTemp = criticalTemp()
	app.use(bodyParser())
	app.use(async ctx => {
		const { sensors } = ctx.request.body as { sensors: string }
		const lines = sensors.split('\n')
		const processorFanLine = lines.find(line => line.includes('Processor Fan:'))
		if (processorFanLine) {
			const fanRpm = parseFloat(processorFanLine.replace(/[^\d]/gi, ''))
			if (fanRpm > 0) {
				if (ctx.coolerState.get() === 'off') {
					ctx.coolerState.set(await setUsbState('on'))
				}
				console.log('Running with fan', fanRpm)
				ctx.body = 'ok'
				return
			}
		}
		const temperatureLines = lines.filter(line => line.includes('°C'))
		const temperatures = temperatureLines.map(line =>
			parseInt(line.slice(-8, -6))
		)
		const highestTemperature = Math.max(...temperatures)
		const cool = highestTemperature < ctx.criticalTemp.get()

		let sensorsState = ctx.sensorsState.get()
		const setSensors = ctx.sensorsState.set
		console.log('===')
		console.log(sensorsState)
		if (!cool) {
			switch (sensorsState) {
				case 'cool':
					setSensors('warm')
					break
				case 'warm':
					setSensors('hot')
					break
				case 'hot':
					setSensors('burn')
					break
				case 'burn':
					break
				default:
					throw new Error('Unknown sensors state: ' + sensorsState)
			}
		} else {
			switch (sensorsState) {
				case 'cool':
					break
				case 'warm':
					setSensors('cool')
					break
				case 'hot':
					setSensors('warm')
					break
				case 'burn':
					setSensors('hot')
					break
				default:
					throw new Error('Unknown sensors state: ' + sensorsState)
			}
		}

		sensorsState = ctx.sensorsState.get()
		switch (sensorsState) {
			case 'burn':
				ctx.criticalTemp.set(65)
				break
			case 'hot':
				ctx.criticalTemp.set(69)
				break
			default:
				break
		}

		switch (sensorsState) {
			case 'burn':
				if (ctx.coolerState.get() === 'off') {
					ctx.coolerState.set(await setUsbState('on'))
				}
				break
			default:
				if (ctx.coolerState.get() === 'on') {
					ctx.coolerState.set(await setUsbState('off'))
				}
				break
		}
		console.log(sensorsState, highestTemperature, JSON.stringify(temperatures))
		ctx.body = highestTemperature
	})

	app.listen(3000, () => console.log('App is listening'))
}

startServer()
