import { spawn } from 'child_process'

import fetch from 'node-fetch'

const getSensorsData = (): Promise<string> => {
	return new Promise((resolve, reject) => {
		const sensors = spawn('sensors')
		sensors.stdout.on('data', (data: Buffer) => {
			resolve(data.toString())
		})
		sensors.stderr.on('data', (data: Buffer) => {
			reject(data.toString())
		})
		sensors.on('close', (code: number) => {
			reject('Exited with code' + code)
		})
	})
}

setInterval(async () => {
	const data = await getSensorsData()
	try {
		await fetch('http://10.0.0.18:3000', {
			headers: {
				'Content-Type': 'application/json',
			},
			method: 'POST',
			body: JSON.stringify({ sensors: data }),
		})
	} catch (e) {
		return void 0
	}
}, 3000)
